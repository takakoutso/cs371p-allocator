// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <string>
#include <sstream>
#include <vector>

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */

    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;

    int t;
    string temp;
    cin >> t;
    getline(cin, temp);
    getline(cin, temp);
    for (int i = 0; i < t; ++i) {
        int val;
        allocator_type a;
        vector<value_type*> ptrs;
        vector<size_type > sizs;

        while (getline(cin, temp)) {
            if (temp == "") break;
            istringstream str(temp);
            str >> val;
            if (val > 0) {
                value_type* ptr = a.allocate(val);

                auto ptrIt = ptrs.begin();
                auto sizIt = sizs.begin();

                bool added = false;
                while (ptrIt != ptrs.end()) {
                    if (ptr < *ptrIt) {
                        ptrs.insert(ptrIt, ptr);
                        sizs.insert(sizIt, val);
                        added = true;
                        break;
                    }
                    ptrIt++;
                    sizIt++;
                }
                if (!added) {
                    ptrs.insert(ptrIt, ptr);
                    sizs.insert(sizIt, val);
                }

            } else {

                int index = (-1 * val) - 1;
                value_type* ptr = ptrs[index];
                size_type   siz = sizs[index];

                ptrs.erase(ptrs.begin() + index);
                sizs.erase(sizs.begin() + index);
                a.deallocate(ptr, siz);

            }
        }
        allocator_type::iterator it = a.begin();
        cout << *it;
        it++;
        while (it != a.end()) {
            cout << " " << *it;
            it++;
        }

        cout << endl;
    }

    return 0;
}
