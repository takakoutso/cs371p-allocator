// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// --------
// defines
// --------

#define MAX_NUM_LINES 1000

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:

    /**
     * getBlock
     * @param p pointer to end of first sentinel
     * returns int* to beginning of first sentinel.
     */
    static int* getBlock(pointer p) {
        char* start = reinterpret_cast<char*>(p);
        start -= 4;
        return reinterpret_cast<int*>(start);
    }

    /**
     * getData
     * @param p int* to start of first sentinel
     * returns pointer to end of first sentinel.
     */
    static pointer getData(int* p) {
        char* start = reinterpret_cast<char*>(p);
        return reinterpret_cast<pointer>(start + 4);
    }

    /**
     * getNextSentinel
     * @param p int* to start of first sentinel
     * returns int* to start of second sentinel.
     */
    static int* getNextSentinel(int* p) {
        char* start    = reinterpret_cast<char*>(p);
        int   sentinel = *p > 0 ? *p : -1 * (*p);
        return reinterpret_cast<int*>(start + sentinel + 4);
    }

    /**
     * getPrevBlock
     * @param p int* to start of first sentinel
     * returns int* to start of first sentinel of previous block
     */
    static int* getPrevBlock(int* p) {
        char* start = reinterpret_cast<char*>(p);

        start -= 4;
        int* prevSent = reinterpret_cast<int*>(start);
        int  sentVal  = *prevSent > 0 ? *prevSent : -1 * (*prevSent);
        start -= sentVal + 4;
        return reinterpret_cast<int*>(start);

    }

    /**
     * getSafePrevBlock
     * @param p int* to start of first sentinel
     * returns int* to start of first sentinel of previous block if exists, else p.
     */
    int* getSafePrevBlock(int* p) {
        char* start = reinterpret_cast<char*>(p);

        if (start > a) {
            start -= 4;

            int* prevSent = reinterpret_cast<int*>(start);
            int  sentVal  = *prevSent > 0 ? *prevSent : -1 * (*prevSent);
            start -= sentVal + 4;
            return reinterpret_cast<int*>(start);
        } else {
            return p;
        }
    }

    static int* getNextBlock(int* p) {
        return reinterpret_cast<int*>(reinterpret_cast<char*>(getNextSentinel(p)) + 4);
    }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:

        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            this->_p = getNextBlock(this->_p);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++(*this);
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            this->_p = getPrevBlock(this->_p);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            this->_p = getNextBlock(const_cast<int*>(this->_p));
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++(*this);
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            this->_p = getPrevBlock(const_cast<int*>(this->_p));
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * simply uses const_iterator to make sure that all of a[N] is traversed in blocks.
     */
    bool valid () const {
        const_iterator it = begin();
        const_iterator ed = end();
        int counter = N/sizeof(T);
        int size = 0;
        bool prevFreed = false;

        while (it != ed) {
            int curSize = *it;
            if (curSize > 0) {
                if (prevFreed) {
                    return false;
                }
                prevFreed = true;
            } else {
                curSize = -1 * curSize;
                prevFreed = false;
            }
            size += curSize + 8;
            it++;
            if (--counter <= 0) {
                return false;
            }
        }
        return size == 1000;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     * initializes allocator with a single large free block
     */
    my_allocator () {
        if (N < sizeof(T) + (2 * sizeof(int))) {
            throw std::bad_alloc();
        }

        int sentinel = N - 2 * sizeof(int);
        int* ptr;

        ptr  = reinterpret_cast<int*>(&a[0]);
        *ptr = sentinel;
        ptr  = reinterpret_cast<int*>(&a[N-4]);
        *ptr = sentinel;

        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        assert(valid());
        n *= sizeof(T);
        if (n <= 0 || n > N - 8) {
            throw std::bad_alloc();
        }
        iterator it = begin();
        iterator ed = end();
        char* p = a;
        while (it != ed) {
            if (*it > 0 && static_cast<size_type>(*it) > 8 + n) { // if we need to split this block into two
                int original_size = *it;
                int siz1= -1 * n;
                int siz2= original_size - 8 - n;

                int* _p = reinterpret_cast<int*>(p);
                int* s1 = _p;                                   // first  sentinel of first  block
                int* s4 = getNextSentinel(_p);                  // second sentinel of second block
                *s1     = siz1;
                *s4     = siz2;
                int* s2 = getNextSentinel(_p);                  // second sentinel of first  block
                int* s3 = getNextBlock(_p);                     // first  sentinel of second block
                *s2     = siz1;
                *s3     = siz2;

                return getData(_p);


            } else if (*it > 0 && static_cast<size_type>(*it) >= n) { // if we dont have enough left over for another block

                int original_size = *it;
                int* _p = reinterpret_cast<int*>(p);
                int* s1 = _p;
                int* s2 = getNextSentinel(_p);

                *s1 = -1 * original_size;
                *s2 = *s1;

                return getData(_p);
            }
            if (*it > 0) {                  // p keeps track of the block we're looking at
                p += *it + 8;
            } else {
                p += -1 * (*it) + 8;
            }
            ++it;
        }
        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * frees requested block and combines with either or both of adjacent blocks if free
     */
    void deallocate (pointer p, size_type n) {
        int* _p   = getBlock(p);
        assert(valid());
        assert(n != 0);
        assert(n * sizeof(T) <= -1 * static_cast<size_type>(*_p));
        int  size = *_p * -1;
        int* sen1 = _p;
        int* sen2 = getNextSentinel(_p);
        int* temp = getSafePrevBlock(_p);

        if (*temp > 0) { // impl. of getSafePrevBlock makes sure we dont go beyond array start
            if (sen1 != temp) {
                size += 8 + *temp; // if not the first block, add the size + sentinels
            }
            sen1  = temp; // if the previous block is also unallocated, its the left sentinel

        }
        temp      = getNextBlock(_p);
        if (reinterpret_cast<char*>(temp) < &a[N] && *temp > 0) { // getNextBlock doesn't bounds check, we must do it
            size += 8 + *temp;
            sen2 = getNextSentinel(temp);
        }

        *sen1 = size;
        *sen2 = size;

    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
