var searchData=
[
  ['getblock',['getBlock',['../classmy__allocator.html#accb69e24ae5f421f7225b2145c0b72fe',1,'my_allocator']]],
  ['getdata',['getData',['../classmy__allocator.html#a191846eadea40c53d7c73c2adbbe585d',1,'my_allocator']]],
  ['getnextblock',['getNextBlock',['../classmy__allocator.html#a0420e3d47d340403c21bf14c40f738a6',1,'my_allocator']]],
  ['getnextsentinel',['getNextSentinel',['../classmy__allocator.html#a4cf8490e1b2641bbc5108d3fc27a51cb',1,'my_allocator']]],
  ['getprevblock',['getPrevBlock',['../classmy__allocator.html#ad5d07a271b2e579d57bc68ea1b2105a8',1,'my_allocator']]],
  ['getsafeprevblock',['getSafePrevBlock',['../classmy__allocator.html#a42a900c7bbf5f81b52248636b61dd44f',1,'my_allocator']]]
];
