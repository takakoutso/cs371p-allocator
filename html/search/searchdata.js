var indexSectionsWithContent =
{
  0: "_abcdegimoprstv~",
  1: "cim",
  2: "art",
  3: "abcdegimotv~",
  4: "_a",
  5: "cdprsv",
  6: "o",
  7: "m",
  8: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "related",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Friends",
  7: "Macros",
  8: "Pages"
};

