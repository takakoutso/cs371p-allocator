// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// uncomment
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);
}                                         // fix test

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}                                         // fix test

TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;

    int sample[] = {1, 2, 3, 4};
    allocator_type a;

    for (int i = 0; i < 4; i++) {
        a.allocate(sample[i]);
    }

    allocator_type::iterator it = a.begin();
    allocator_type::iterator ed = a.end();

    ed--;
    int index = 0;
    while (it != ed) {
        ASSERT_EQ(true, index < 4);
        int val = *(it++) * -1;
        ASSERT_EQ(val, sample[index++] * sizeof(int));
    }

}

TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;

    int sample[] = {1, 2, 3, 4, -920};
    allocator_type a;

    for (int i = 0; i < 4; i++) {
        a.allocate(sample[i]);
    }

    allocator_type::iterator it = a.begin();
    allocator_type::iterator ed = a.end();

    ed--;

    int index = 3;
    while (it != ed) {
        ASSERT_EQ(true, index >= 0);
        int val = *(--ed) * -1;
        ASSERT_EQ(val, sample[index--] * sizeof(int));
    }

}


TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type a;

    int count = 0;
    while (a.allocate(1)) {
        count++;
    }

    ASSERT_EQ(count, 83);
}

TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type a;

    int* arr[83];

    for (int i = 0; i < 83; i++) {
        arr[i] = a.allocate(1);
    }
    for (int i = 0; i < 83; i++) {
        a.deallocate(arr[i], 1);
    }

    allocator_type::iterator it = a.begin();

    ASSERT_EQ(*it, 992);
}


TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type a;

    int count = 0;
    while (a.allocate(count + 1)) {
        count++;
    }

    ASSERT_EQ(count, 20);
}

TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type a;

    pointer arr[83];

    for (int i = 0; i < 83; i++) {
        arr[i] = a.allocate(1);
    }
    for (int i = 0; i < 83; i++) {
        a.deallocate(arr[i], 1);
    }

    for (int i = 0; i < 83; i++) {
        arr[i] = a.allocate(1);
    }
    for (int i = 0; i < 83; i++) {
        a.deallocate(arr[i], 1);
    }

    allocator_type::iterator it = a.begin();

    ASSERT_EQ(*it, 992);
}

TEST(AllocatorFixture, test10) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type a;
    allocator_type b;

    a.allocate(1);
    b.allocate(1);

    allocator_type::iterator it1 = a.begin();
    allocator_type::iterator it2 = a.end();
    ----it2;
    allocator_type::iterator it3 = b.begin();
    allocator_type::iterator it4 = b.end();
    ----it4;

    bool result = (it1 == it2) && (it3 == it4) && (it1 != it3);

    ASSERT_EQ(result, true);
}

TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<double, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type a;

    pointer ptr = a.allocate(5);
    a.allocate(1);

    a.deallocate(ptr, 5);
    a.allocate(2);
    a.allocate(2);

    allocator_type::iterator it = a.begin();
    allocator_type::iterator ed = a.end();

    int sample[] = {-16, -16, -8, 928};
    int index = 0;
    while (it != ed) {
        ASSERT_EQ(sample[index++], *(it++));
    }

}

TEST(AllocatorFixture, test12) {
    using allocator_type = my_allocator<double, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type a;

    pointer ptr1 = a.allocate(124);
    pointer ptr2 = a.allocate(1);

    ASSERT_EQ(ptr1 == nullptr, false);
    ASSERT_EQ(ptr2, nullptr);

}

TEST(AllocatorFixture, test13) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type a;

    pointer ptr;
    for (int i = 0; i < 83; i++) {
        ptr = a.allocate(1);
    }

    ASSERT_EQ(ptr == nullptr, false);

    ptr = a.allocate(1);

    ASSERT_EQ(ptr, nullptr);

}

TEST(AllocatorFixture, test14) {
    using allocator_type = my_allocator<int, 1000>;

    const allocator_type a;


    allocator_type::const_iterator it = a.begin();
    allocator_type::const_iterator ed = a.end();

    it++;

    ASSERT_EQ(it, ed);

}

TEST(AllocatorFixture, test15) {
    using allocator_type = my_allocator<int, 1000>;

    const allocator_type a;


    allocator_type::const_iterator it = a.begin();
    allocator_type::const_iterator ed = a.end();

    ++it;

    ASSERT_EQ(it, ed);

}

TEST(AllocatorFixture, test16) {
    using allocator_type = my_allocator<int, 1000>;

    const allocator_type a;


    allocator_type::const_iterator it = a.begin();
    allocator_type::const_iterator ed = a.end();

    ed--;

    ASSERT_EQ(it, ed);

}

TEST(AllocatorFixture, test17) {
    using allocator_type = my_allocator<int, 1000>;

    const allocator_type a;


    allocator_type::const_iterator it = a.begin();
    allocator_type::const_iterator ed = a.end();

    --ed;

    ASSERT_EQ(it, ed);

}

TEST(AllocatorFixture, test18) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 100;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}



TEST(AllocatorFixture, test19) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 83;
    const value_type v = 1;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}